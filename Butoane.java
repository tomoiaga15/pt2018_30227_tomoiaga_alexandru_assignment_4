import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

public class Butoane implements ActionListener {
    private int id;
    private JTextField s1, s2, s3, s4, s5, s6, s7;
    private JTable t1;

    public Butoane(int id, JTextField s1, JTextField s2, JTextField s3, JTextField s4) {
        this.id = id;
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
        this.s4 = s4;
    }
    public Butoane(int id, JTextField s1, JTextField s2, JTextField s3, JTextField s4, JTextField s5) {
        this.id = id;
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
        this.s4 = s4;
        this.s5=s5;
    }

    public Butoane(int id, JTextField s1, JTextField s2, JTextField s3, JTextField s4, JTextField s5, JTextField s6) {
        this.id = id;
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
        this.s4 = s4;
        this.s5=s5;
        this.s6=s6;
    }

    public Butoane(int id, JTextField s1, JTextField s2, JTextField s3, JTextField s4, JTextField s5, JTextField s6, JTextField s7) {
        this.id = id;
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
        this.s4 = s4;
        this.s5 = s5;
        this.s6 = s6;
        this.s7 = s7;
    }

    public Butoane(int id, JTable t1){
        this.id=id;
        this.t1=t1;
    }
    public Butoane(int id, JTextField s1, JTextField s2, JTextField s3, JTextField s4, JTable t1) {
        this.id = id;
        this.id = id;
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
        this.s4 = s4;
        this.t1 = t1;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        Bank b=new Bank("Banca");
        Tabel t=new Tabel();
        Person p;
        SavingAccount savingAccount;
        SpendingAccount spendingAccount;
        switch (id) {
            case 0:
                p=new Person(Integer.parseInt(s1.getText()), s2.getText(), Integer.parseInt(s3.getText()), Integer.parseInt(s4.getText()));
                try {
                    b.addPerson(p);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                p=new Person(Integer.parseInt(s1.getText()), s2.getText(), Integer.parseInt(s3.getText()), Integer.parseInt(s4.getText()));
                spendingAccount=new SpendingAccount(Integer.parseInt(s5.getText()),Integer.parseInt(s1.getText()));
                try {
                    b.addAccount(spendingAccount, p);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                p=new Person(Integer.parseInt(s1.getText()), s2.getText(), Integer.parseInt(s3.getText()), Integer.parseInt(s4.getText()));
                savingAccount=new SavingAccount(Integer.parseInt(s5.getText()), Integer.parseInt(s1.getText()), Integer.parseInt(s6.getText()),Integer.parseInt(s7.getText()));
                try {
                    b.addAccount(savingAccount, p);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                p=new Person(Integer.parseInt(s1.getText()), s2.getText(), Integer.parseInt(s3.getText()), Integer.parseInt(s4.getText()));
                try {
                    b.deletePerson(p);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 4:
                p=new Person(Integer.parseInt(s1.getText()), s2.getText(), Integer.parseInt(s3.getText()), Integer.parseInt(s4.getText()));
                b.deleteAccount(Integer.parseInt(s5.getText()), p);
                break;
            case 5:
                List<Person> list=new ArrayList<>();
                list=b.viewPerson();
                try {
                    new Vizualizare("Vizualizare Persoane",personToObj(list));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case 6:
                p=new Person(Integer.parseInt(s1.getText()), s2.getText(), Integer.parseInt(s3.getText()), Integer.parseInt(s4.getText()));
                try {
                    new Vizualizare("Vizualizare Conturi",accountToObj(b.viewAccount(p)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();}
                break;
            case 7:
                p=new Person(Integer.parseInt(s1.getText()), s2.getText(), Integer.parseInt(s3.getText()), Integer.parseInt(s4.getText()));
                try {
                    b.addMoney(p, Integer.parseInt(s5.getText()), Integer.parseInt(s6.getText()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 8:
                p=new Person(Integer.parseInt(s1.getText()), s2.getText(), Integer.parseInt(s3.getText()), Integer.parseInt(s4.getText()));
                try {
                    b.withdrawMoney(p, Integer.parseInt(s5.getText()), Integer.parseInt(s6.getText()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
    public List<Object> personToObj(List<Person> person) {
        List<Object> list = new ArrayList<>();
        for(Person p : person){
            list.add(p);
        }
        return list;
    }

    public List<Object> accountToObj(List<Account> account) {
        List<Object> list = new ArrayList<>();
        for(Account a : account){
            list.add(a);
        }
        return list;
    }
}
