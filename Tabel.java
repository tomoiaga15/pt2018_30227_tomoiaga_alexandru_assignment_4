import javax.swing.*;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;


public class Tabel {

    private Vector<String> header;
    private Vector<Vector> list;
    public JTable retrieveProperties(List<Object> object) throws IllegalAccessException {
        header=new Vector<String>();
        list=new Vector<Vector>();
        for (Field field : object.get(0).getClass().getDeclaredFields()) {
            field.setAccessible(true); // set modifier to public
            try {
                header.add(field.getName());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        for(int i=0;i<object.size();i++) {
            Vector<Object> v=new Vector<Object>();
            for(Field field: object.get(i).getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    Object a = field.get(object.get(i));
                    v.add(a);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
            list.add(v);
        }
        return new JTable(list, header);
    }
}
