public class Testare {
    Bank b=new Bank("Banca");

    public boolean validarePersoana(Person p) throws Exception {
        b.addPerson(p);
        Person p1= b.getPersonById(p.getId());
        if(p.equals(p1))
            return true;
        else
            return false;
    }

    public boolean validareAccount(Person p, Account a) throws Exception {
        b.addAccount(a, p);
        Account a1=b.getAccountById(p, a.getId());
        if(a.equals(a1))
            return true;
        else
            return false;
    }

    public boolean validareUpdate(Person p, int amount, int id) throws Exception {
        int ramas = b.getAccountById(p, id).getAmount() + amount;
        b.addMoney(p, amount, id);
        if (ramas == b.getAccountById(p, id).getAmount())
            return true;
        else return false;
    }

    public boolean validareDeletePerson(Person p) throws Exception {
        b.deletePerson(p);
        if(b.getPersonById(p.getId())==null)
            return true;
        else
            return false;
    }

    public boolean validareDeleteAccount(Person p, int idA) throws Exception {
        b.deleteAccount(idA, p);
        if(b.getAccountById(p, idA)==null)
            return true;
        else
            return false;
    }
}
