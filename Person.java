public class Person {

    private int id, telefon;
    private String nume;
    private long cnp;

    public Person(int id, String nume, long cnp, int telefon) {
        this.id = id;
        this.nume = nume;
        this.cnp = cnp;
        this.telefon = telefon;
    }

    public int getTelefon() {
        return telefon;
    }

    public void setTelefon(int telefon) {
        this.telefon = telefon;
    }

    public long getCnp() {
        return cnp;
    }

    public void setCnp(long cnp) {
        this.cnp = cnp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", telefon=" + telefon +
                ", nume='" + nume + '\'' +
                ", cnp=" + cnp +
                '}';
    }
}
