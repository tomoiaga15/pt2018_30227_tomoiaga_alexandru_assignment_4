import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

class Start extends JFrame {


        JPanel panel1, panel2, panel3, panel4, panel5, panel6, panel7, panel8, panel9;
        JLabel l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11;
        JTextField tf1, tf2, tf3, tf4, tf5, tf6, tf7, tf8, tf9, tf10, tf11;
        JButton b1, b2, b3, b4, b5, b6, b7, b8, b9;
        JTable tabel1, tabel2;

        public Start(String nume) {
            setTitle(nume);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(1000, 680);

            this.addWindowListener(new WindowListener() {
                @Override
                public void windowOpened(WindowEvent e) {

                }

                @Override
                public void windowClosing(WindowEvent e) {

                }

                @Override
                public void windowClosed(WindowEvent e) {

                }

                @Override
                public void windowIconified(WindowEvent e) {

                }

                @Override
                public void windowDeiconified(WindowEvent e) {

                }

                @Override
                public void windowActivated(WindowEvent e) {

                }

                @Override
                public void windowDeactivated(WindowEvent e) {

                }
            });
            panel1 = new JPanel();
            panel2 = new JPanel();
            panel3 = new JPanel();
            panel4 = new JPanel();
            panel5 = new JPanel();
            panel6 = new JPanel();
            panel7 = new JPanel();
            panel8 = new JPanel();
            panel9 = new JPanel();

            l1 = new JLabel("ID Persoana:");
            l2 = new JLabel("Nume:");
            l3 = new JLabel("CNP:");
            l4 = new JLabel("Telefon:");
            l5 = new JLabel("ID SpendingA:");
            l6 = new JLabel("ID SavingA:");
            l7 = new JLabel("Dobanda:");
            l8 = new JLabel("Perioada:");
            l9 = new JLabel("ID AccToBeDeleted:");
            l10 = new JLabel("ID AccToAddMoneyTo:");
            l11 = new JLabel("Amount:");

            tf1 = new JTextField("0");
            tf2 = new JTextField("-");
            tf3 = new JTextField("0");
            tf4 = new JTextField("0");
            tf5 = new JTextField("0");
            tf6 = new JTextField("0");
            tf7 = new JTextField("0");
            tf8 = new JTextField("0");
            tf9 = new JTextField("0");
            tf10 = new JTextField("0");
            tf11 = new JTextField("0");

            tf1.setColumns(5);
            tf2.setColumns(25);
            tf3.setColumns(15);
            tf4.setColumns(13);
            tf5.setColumns(5);
            tf6.setColumns(5);
            tf7.setColumns(5);
            tf8.setColumns(5);
            tf9.setColumns(5);
            tf10.setColumns(5);
            tf11.setColumns(5);


            panel1.setLayout(new FlowLayout());
            panel2.setLayout(new FlowLayout());
            panel3.setLayout(new FlowLayout());
            panel4.setLayout(new FlowLayout());
            panel5.setLayout(new FlowLayout());
            panel6.setLayout(new FlowLayout());
            panel7.setLayout(new FlowLayout());
            panel8.setLayout(new FlowLayout());
            //panel9.setLayout(new FlowLayout());
            // panel10.setLayout(new FlowLayout());
            //panel11.setLayout(new FlowLayout());

            b1 = new JButton("Add Person");
            b2 = new JButton("Add Spending Account");
            b3 = new JButton("Add Saving Account");
            b4 = new JButton("Delete Person");
            b5 = new JButton("Delete Account");
            b6 = new JButton("View Person List");
            b7 = new JButton("View Account per Person");
            b8 = new JButton("Add Money");
            b9 = new JButton("Withdraw Money");


            panel1.add(b1);
            panel2.add(b2);
            panel3.add(b3);
            panel4.add(b4);
            panel5.add(b5);
            panel6.add(b6);
            panel7.add(b7);
            panel8.add(b8);
            panel9.add(b9);

            panel1.add(l1);
            panel1.add(tf1);
            panel1.add(l2);
            panel1.add(tf2);
            panel1.add(l3);
            panel1.add(tf3);
            panel1.add(l4);
            panel1.add(tf4);
            panel2.add(l5);
            panel2.add(tf5);
            panel3.add(l6);
            panel3.add(tf6);
            panel3.add(l7);
            panel3.add(tf7);
            panel3.add(l8);
            panel3.add(tf8);
            panel5.add(l9);
            panel5.add(tf9);
            panel8.add(l10);
            panel8.add(tf10);
            panel8.add(l11);
            panel8.add(tf11);

            b1.addActionListener(new Butoane(0, tf1, tf2, tf3, tf4));
            b2.addActionListener(new Butoane(1, tf1, tf2, tf3, tf4, tf5));
            b3.addActionListener(new Butoane(2, tf1, tf2, tf3, tf4, tf6, tf7, tf8));
            b4.addActionListener(new Butoane(3, tf1, tf2, tf3, tf4));
            b5.addActionListener(new Butoane(4, tf1, tf2, tf3, tf4, tf9));
            b6.addActionListener(new Butoane(5, tabel1));
            b7.addActionListener(new Butoane(6, tf1, tf2, tf3, tf4, tabel2));
            b8.addActionListener(new Butoane(7, tf1, tf2, tf3, tf4, tf10, tf11));
            b9.addActionListener(new Butoane(8, tf1, tf2, tf3, tf4, tf10, tf11));


            JPanel p = new JPanel();
            p.add(panel1);
            p.add(panel2);
            p.add(panel3);
            p.add(panel4);
            p.add(panel5);
            p.add(panel6);
            p.add(panel7);
            p.add(panel8);
            p.add(panel9);
            //panel9=new JPanel();
            //panel9.add(panel10);
            // panel9.add(panel11);
            p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
            setContentPane(p);
            setVisible(true);
        }

        public static void main(String args[]) {
            new Start("Pagina principala");
        }

    }
