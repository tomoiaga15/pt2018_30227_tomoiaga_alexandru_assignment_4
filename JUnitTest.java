import static org.junit.Assert.assertTrue;

public class JUnitTest{
    public void check() throws Exception {
        Testare t=new Testare();

        Person p1=new Person(1, "nume", 123, 123);
        Person p2=new Person(2, "nume", 123, 123);
        SpendingAccount sa=new SpendingAccount(2, 1);
        SavingAccount saa=new SavingAccount(1, 2, 2, 3);

        assertTrue(t.validarePersoana(p1));
        assertTrue(t.validarePersoana(p2));

        assertTrue(t.validareAccount(p1, sa));
        assertTrue(t.validareAccount(p2, saa));

        assertTrue(t.validareUpdate(p1, 200, 2));

        assertTrue(t.validareDeleteAccount(p2, 1));
        assertTrue(t.validareDeletePerson(p2));
    }
}