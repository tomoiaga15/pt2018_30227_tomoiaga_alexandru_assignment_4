public class Account {
    private int id;
    private int amount;
    private int ownedId;
    private int tip;
    private int dobanda;
    private int perioada;

    public Account(){
    }

    public Account(int id, int amount, int ownedId, int tip, int dobanda, int perioada) {
        this.id = id;
        this.amount = amount;
        this.ownedId = ownedId;
        this.tip=tip;
        this.dobanda=dobanda;
        this.perioada=perioada;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getOwnedId() {
        return ownedId;
    }

    public void setOwnedId(int ownedId) {
        this.ownedId = ownedId;
    }
    public int getTip() {
        return tip;
    }

    public void setTip(int tip) {
        this.tip = tip;
    }

    public int getDobanda() {
        return dobanda;
    }

    public void setDobanda(int dobanda) {
        this.dobanda = dobanda;
    }

    public int getPerioada() {
        return perioada;
    }

    public void setPerioada(int perioada) {
        this.perioada = perioada;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", amount=" + amount +
                ", ownedId=" + ownedId +
                ", tip=" + tip +
                ", dobanda=" + dobanda +
                ", perioada=" + perioada +
                '}';
    }
}
