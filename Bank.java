import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Bank implements BankProc {
    private HashMap<Person, List<Account>>ht;
    private String nume;

    public Bank(String nume){
        ht = new HashMap();
        this.nume=nume;
    }

    public void addPerson(Person p) throws Exception {
        assert(!p.equals(null));
        if (ht.containsKey(p)) {
            throw new Exception("The person already exist.");
        } else {
            if (p.getCnp() < 1000000000 && p.getCnp() > 1999999999) {
                throw new Exception("CNP is wrong.");
            } else if (p.getTelefon() < 700000000 && p.getTelefon() > 799999999) {
                throw new Exception("Phone number is wrong.");
            } else {
                ht.put(p, new ArrayList<Account>());
                System.out.println("The Person with id = " + p.getId() + " has been added.");
            }
        }
    }

    public void deletePerson(Person p) throws Exception {
        assert(!p.equals(null));
        if (ht.containsKey(p)) {
            List<Account> list;
            list = ht.get(p);
            if (list.isEmpty()) {
                ht.remove(p);
                System.out.println("The Person with id = " + p.getId() + " has been deleted.");
            } else
                throw new Exception("Please delete related accounts first.");
        } else
            throw new Exception("Person does not exist.");
    }

    public void addAccount(Account a, Person p) throws Exception {
        assert(!p.equals(null) && !a.equals(null));
        List<Account> list;
        list = ht.get(p);
        int ok=0;
        for(Account acc:list){
            if(acc.getId()==a.getId())
                ok=1;
        }
        if(ok==0){
        list.add(a);
        ht.put(p, list);
            System.out.println("The account with id = "+a.getId()+" has been added.");}
        else
            throw new Exception("Account id is already in use for this account.");
    }

    public void deleteAccount(int idA, Person p) {
        assert(!p.equals(null) && idA!=0);
        List<Account> list;
        list = ht.get(p);
        for(Account a:list)
            if(a.getId()==idA)
            {
                list.remove(a);
                break;
            }
        ht.put(p, list);
        System.out.println("The account with id = "+idA+" has been deleted.");
    }

    public void withdrawMoney(Person p, int amount, int id) throws Exception {
        assert(!p.equals(null) && amount>0 && id!=0);
        List<Account> list;
        list = ht.get(p);
        for (Account a : list) {
            if (a.getId() == id) {
                if (a.getTip() == 0) {
                    if (amount < a.getAmount()) {
                        a.setAmount(a.getAmount() - amount);
                        System.out.println("The exctracted amount is:" + amount);
                    } else {
                        throw new Exception("Invalid amount.");
                    }
                }
                else
                {
                    if(a.getAmount()>0) {
                        int value = a.getAmount();
                        for (int i = 0; i < a.getPerioada(); i++) {
                            value = value * (100 + a.getDobanda()) / 100;
                        }
                        System.out.println("The exctracted amount is:" + value);
                        a.setAmount(-1);
                    }
                    else if(a.getAmount()==0){
                        throw new Exception("The account is empty.");
                    }
                    else
                        throw new Exception("There has been a withdraw already.");
                }
            }
        }
    }

    public void addMoney(Person p, int amount, int id) throws Exception {
        assert(!p.equals(null) && amount>0 && id!=0);
        List<Account> list;
        list = ht.get(p);
        for (Account a : list) {
            if (a.getId() == id) {
                {
                    if (a.getTip() == 0) {
                        a.setAmount(a.getAmount() + amount);
                        System.out.println("The amount = "+amount+" has been added to the account with id = " + id+  ".");
                    } else {
                        if (amount >= 1000 && a.getAmount() == 0) {
                            a.setAmount(amount);
                            System.out.println("The amount = "+amount+" has been added to the account with id = " + id+  ".");
                        } else {
                            if (amount < 1000) {
                                throw new Exception("Amount is less than the limit permitted.");
                            } else {
                                if (a.getAmount() != 0) {
                                    throw new Exception("There has been an add already.");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public List<Person> viewPerson() {
        List<Person> list = new ArrayList<>(ht.keySet());
        return list;
    }

    public List<Account> viewAccount(Person p) {
        List<Account> list;
        list = ht.get(p);
        return list;
    }

    public Person getPersonById(int id){
        List<Person> list = new ArrayList<>(ht.keySet());
        for(Person p:list)
            if(p.getId()==id)
                return p;
        return null;
    }

    public Account getAccountById(Person p, int id){
        List<Account> list;
        list = ht.get(p);
        for(Account a:list)
            if(a.getId()==id)
                return a;
        return null;
    }
}
