import java.util.List;

public interface BankProc {

    //@pre p!=null
    public void addPerson(Person p) throws Exception;
    //@post p a fost inserata

    //@pre p!=null
    public void deletePerson(Person p) throws Exception;
    //@post p a fost sters

    //@pre p!=null si a!=null
    public void addAccount(Account a, Person p) throws Exception;
    //@post a a fost inserat

    //@pre p!=null si idA!=0
    public void deleteAccount(int idA, Person p);
    //@post contul cu id idA a fost sters

    //@pre p!=null amount >0, id !=0
    public void withdrawMoney(Person p, int amount, int id)throws Exception ;
    //@post suma a fost extrasa si contul actualizat

    //@pre p!=null amount >0, id !=0
    public void addMoney(Person p, int amount, int id)throws Exception ;
    //@post suma a fost adaugata si contul a fost actualizat
}
